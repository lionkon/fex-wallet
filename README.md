# FexCoin钱包是一款存储加密货币的手机钱包, 属于区块链数字资产管理工具

#### 特别声明
本软件属于技术开源软件, 任何使用本源码从事商业活动，对别人和自己造成损失的，本人概不负责！

### Android测试包下载: [下载](https://fexcoin.oss-cn-shenzhen.aliyuncs.com/upload/apk/fexcoin.apk)


#### 技术交流社区

##### [![微信群:](https://img.shields.io/badge/%E5%BE%AE%E4%BF%A1-fexcoin-green)]()&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[![QQ群:](https://img.shields.io/badge/QQ%E7%BE%A4-731992394-green)](https://jq.qq.com/?_wv=1027&k=mN37oSXN)
![Image text](doc/wechat_qrcode.jpg) 
![Image text](doc/qq_qrcode.jpg) 


#### 功能规划
1. 资产托管
2. 闪电兑换
3. 法币交易
4. 行情资讯
5. 存币理财
6. 数字商城

#### 版本更新
##### 2020-06-14: 接入火币K线行情
![Image text](doc/12.jpg) 

![Image text](doc/fexcoin.jpg) 


#### 安装教程

1. 下载HbuilderX
2. 手机USB连接电脑
3. 打开开发者模式, USB调试
4. 打开HbuilderX, 选择 运行 -> 运行到手机或模拟器 -> 选择连接的手机  等待安装

#### 使用说明

1. 将项目拖入[HbuilderX](http://www.dcloud.io/hbuilderx.html) 
2. 运行命令 

   npm install pako

   npm install uview-ui

   npm install @dcloudio/uni-ui

   npm install vue-moment
3. [uni-app教程](https://uniapp.dcloud.io) 

#### 参与贡献

暂无

